/*
 *  Radio Helsinki avr projects
 *
 *
 *  Copyright (C) 2013-2015 Christian Pointner <equinox@helsini.at>
 *
 *  This file is part of Radio Helsinki avr projects.
 *
 *  Radio Helsinki avr projects is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  Radio Helsinki avr projects is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Radio Helsinki avr projects. If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <stdio.h>

#include "util.h"
#include "led.h"
#include "anyio.h"

#define ATS_PIN PINB
#define ATS_PORT PORTB
#define ATS_DDR DDRB
#define ATS_IN_A_SELECTED 0
#define ATS_IN_B_SELECTED 1
#define ATS_IN_A_OK 2
#define ATS_IN_B_OK 3
#define ATS_STATUS 7
#define ATS_LP_MAX 10000

void ats_init(void)
{
  ATS_DDR = 0x00;
  ATS_PORT = 0xFF;
}

inline char* ats_get_ats_status(uint8_t stat)
{
  return (stat & (1<<ATS_STATUS)) ? "Error" : "Ok";
}

inline char* ats_get_selected(uint8_t stat)
{
  stat &= (1<<ATS_IN_A_SELECTED) | (1<<ATS_IN_B_SELECTED);
  switch(stat) {
  case (1<<ATS_IN_A_SELECTED): return "A";
  case (1<<ATS_IN_B_SELECTED): return "B";
  default: return "?";
  }
}

inline char* ats_get_input_a(uint8_t stat)
{
  return (stat & (1<<ATS_IN_A_OK)) ? "Offline" : "Online";
}

inline char* ats_get_input_b(uint8_t stat)
{
  return (stat & (1<<ATS_IN_B_OK)) ? "Offline" : "Online";
}

void ats_print_status(uint8_t stat)
{
  printf("Status: %s, Input %s is selected (A: %s, B: %s)\r\n",
         ats_get_ats_status(stat), ats_get_selected(stat), ats_get_input_a(stat), ats_get_input_b(stat));
}

uint8_t ats_get_status(void)
{
  static uint8_t last_stat = 0;
  static uint16_t lp_cnt = 0;

  uint8_t stat = ATS_PIN;
  if(stat != last_stat)
    lp_cnt++;
  else
    lp_cnt += lp_cnt ? -1 : 0;

  if(lp_cnt >= ATS_LP_MAX) {
    last_stat = stat;
    lp_cnt = 0;
  }

  return last_stat;
}

void ats_task(void)
{
  static uint8_t old_stat = 0;

  uint8_t new_stat = ats_get_status();
  if(old_stat != new_stat) {
    ats_print_status(new_stat);
    old_stat = new_stat;
  }
}

void handle_cmd(uint8_t cmd)
{
  switch(cmd) {
  case '0': led_off(); printf("ok\r\n"); break;
  case '1': led_on(); printf("ok\r\n"); break;
  case 't': led_toggle(); printf("ok\r\n"); break;
  case 's': ats_print_status(ATS_PIN); break;
        /* case 'r': reset2bootloader(); break; */ // for testing only
  default: printf("error: unknown command\r\n"); return;
  }
}

int main(void)
{
  MCUSR &= ~(1 << WDRF);
  wdt_disable();

  cpu_init();
  led_init();
  ats_init();
  anyio_init(115200, 0);
  sei();

  for(;;) {
    int16_t bytes_received = anyio_bytes_received();
    while(bytes_received > 0) {
      int received_byte = fgetc(stdin);
      if(received_byte != EOF) {
        handle_cmd(received_byte);
      }
      bytes_received--;
    }

    anyio_task();
    ats_task();
  }
}
