#!/usr/bin/perl -w
##
##  Radio Helsinki avr projects
##
##
##  Copyright (C) 2013-2015 Christian Pointner <equinox@helsini.at>
##
##  This file is part of Radio Helsinki avr projects.
##
##  Radio Helsinki avr projects is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  Radio Helsinki avr projects is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with Radio Helsinki avr projects. If not, see <http://www.gnu.org/licenses/>.
##

use strict;
use Device::SerialPort;
use Mail::Sendmail;

my $port = new Device::SerialPort("/dev/ats-watch");
$port->baudrate(38400);
$port->parity("none");
$port->databits(8);
$port->stopbits(1);
$port->handshake("none");
$port->read_const_time(500);
$port->read_char_time(5);
$port->write_settings;

$port->lookclear;
$port->write("s");

my %mail = ( To      => 'logs@helsinki.at',
             From    => 'noreply@helsinki.at',
           );

while(1) {
  my ($count, $line) = $port->read(100);
  if($count > 0) {
    if($line =~ /Status: (\w+), Input (\w) is selected \(A: (\w+), B: (\w+)\)/) {
      my $state = $1;
      if($3 ne 'Online' && $4 ne 'Online') {
        $state = 'critical';
      } elsif($3 ne 'Online' || $4 ne 'Online') {
        $state = 'degraded';
      }
      $mail{Subject} = "[ATS-Watch] PDU0 state $state";
      $mail{Message} = "Current State: $1\n" . "Current Input: $2\n" . "Input A: $3\n" . "Input B: $4\n";
    } else {
      $mail{Subject} = '[ATS-Watch] PDU0 state unknown';
      $mail{Message} = $line;
    }
    sendmail( %mail ) or print $Mail::Sendmail::error . "\n";
  }
}
